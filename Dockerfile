FROM ubuntu:20.04
# FROM python:3.12-rc-slim-bullseye

# ENV WORKDIR=/root

# Desactivation du reglage creneaux horaire
ARG DEBIAN_FRONTEND=noninteractive

# Mise à jour ubuntu
RUN apt-get -y update \
&& apt-get -y upgrade

# Installation de python et de pip
RUN apt-get -y install pip

# Installation ssh
RUN apt install -y ssh sshpass git

# Installation de ansible
RUN pip install ansible

# Ansible - Desactivation du host_key_checking
RUN mkdir /etc/ansible \
&& echo "[defaults]" >> /etc/ansible/ansible.cfg \
&& echo "host_key_checking = False" >> /etc/ansible/ansible.cfg

# SSH - Desactivation du host_key_checking
RUN mkdir -p $WORKDIR/.ssh \
&& echo "Host *" > $WORKDIR/.ssh/config \
&& echo "    StrictHostKeyChecking no" >> $WORKDIR/.ssh/config

# # Declaration de l'espace de travail
# WORKDIR $WORKDIR

